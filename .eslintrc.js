// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  extends: 'airbnb-base',
  plugins: [
    'html', // NOTE: required to lint *.vue files
    'chai-friendly'
  ],
  // check if imports actually resolve
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'build/webpack.base.conf.js'
      }
    }
  },
  // add your custom rules here
  'rules': {
    'semi': ['off', 'always'],
    'comma-dangle': ['error', 'never'],
    'no-param-reassign': ["off", { "props": true }],
    // don't require .vue extension when importing
    'import/extensions': ['error', 'always', {
      'js': 'never',
      'vue': 'never'
    }],
    // allow optionalDependencies
    'import/no-extraneous-dependencies': ['error', {
      'optionalDependencies': ['test/unit/index.js']
    }],

    'max-len': [
      'warn',
      240
    ],

    // NOTE: overrides no-unused-expressions to make it friendly towards chai expect statements.
    "no-unused-expressions": 0,
    "chai-friendly/no-unused-expressions": 2,

    // NOTE: application is unusable on Windows machines with wrong text editor settings
    "linebreak-style": process.env.NODE_ENV === 'production' ? 2 : 0,


    // allow console logs during development
    'no-console': process.env.NODE_ENV === 'production' ? 2 : 0,

    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
