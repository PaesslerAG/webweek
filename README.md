# Nürnberg Web Week 2017
## Workshop & Vorträge: Build, Pack, Deploy - From Scratch to Production in No Time

> Moderne Web Applikationen mit Vue, Webpack, Firebase und Serverless

Javascript Framework: [Vue.js](https://vuejs.org/) with [Vuex](https://github.com/vuejs/vuex)  
CSS Framework: [Bulma](http://bulma.io)  
Realtime database (Backend/API): [Firebase](https://firebase.google.com)  
Firebase Library: [Vuefire](https://github.com/vuejs/vuefire)  
AWS Framework: [Serverless](https://serverless.com/)  


## Dev Setup Mac OS X
https://changelog.com/posts/install-node-js-with-homebrew-on-os-x

``` bash
# install Homebrew
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

# install Node.js
brew install node

# install Chromedriver
brew install chromedriver
```

## Dev Setup Windows 10

``` bash
# install Chocolatey (https://chocolatey.org/install)
@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

# install Node.js (https://chocolatey.org/packages/nodejs.install)
choco install nodejs.install

# install Chromedriver (https://chocolatey.org/packages/chromedriver)
choco install chromedriver
```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

## AWS S3 bucket provisioning

``` bash
serverless deploy
```

## Static assets deployment

``` bash
serverless s3deploy
```

For detailed explanation on how webpack work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

Nightwatch - Chrome setup
https://github.com/nightwatchjs/nightwatch/wiki/Chrome-Setup
http://nightwatchjs.org/guide/#writing-tests
