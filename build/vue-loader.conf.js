var utils = require('./utils')
var config = require('../config')
var isProduction = process.env.NODE_ENV === 'production'

module.exports = {
  loaders: utils.cssLoaders({
    sourceMap: isProduction
      ? config.build.productionSourceMap
      : config.dev.cssSourceMap,
    extract: isProduction,
    bulmaLoader: {
      theme: 'sass/bulma.sass'
    },
    plugins: [
      {
        test: /\.scss$/,
        loaders: ["style", "css?modules&importLoaders=2", "sass", "bulma"]
      }
    ]
  }),
  postcss: [
    require('autoprefixer')({
      browsers: ['last 2 versions']
    })
  ]
}
