// NOTE: Visit Firebase setup page (https://firebase.google.com/docs/web/setup)
module.exports = {
  apiKey: null,
  authDomain: 'webweek-98e0f.firebaseapp.com',
  databaseURL: 'https://webweek-98e0f.firebaseio.com',
  projectId: 'webweek-98e0f',
  storageBucket: 'webweek-98e0f.appspot.com',
  messagingSenderId: '979597460395',
}
