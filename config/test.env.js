var merge = require('webpack-merge')
var devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  FIREBASE_API_KEY: JSON.stringify(process.env.FIREBASE_API_KEY)
})
