let config = null

/* eslint-disable */
if(process.env.FIREBASE_API_KEY) {
  // FIXME: Insecure since apiKey is visible in production bundle
  config = require('../config/firebase.example')
  config.apiKey = process.env.FIREBASE_API_KEY
} else {
  config = require('../config/firebase')
}

const Firebase = require('firebase')
const firebaseApp = Firebase.initializeApp(config)
const db = firebaseApp.database()

export default db
/* eslint-enable */
