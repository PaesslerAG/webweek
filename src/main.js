// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueFire from 'vuefire'
import App from './App'
import store from './vuex/store'
import router from './router'

Vue.use(VueFire)

// Aufgabe #6
require('bulma/css/bulma.css')
require('./assets/helpers.css')

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
