import Vue from 'vue'
import VueRouter from 'vue-router'
import Welcome from 'components/Welcome'
import Projects from 'components/Projects'
import UserProfile from 'components/UserProfile'
import store from 'vuex/store'

Vue.use(VueRouter);

const authRoutePath = '/'
const routes = [
  {
    path: authRoutePath,
    name: 'Welcome',
    component: Welcome
  },

  {
    path: '/projects',
    name: 'Projects',
    component: Projects,
    requiresAuth: true
  },

  {
    path: '/user',
    name: 'User',
    component: UserProfile,
    requiresAuth: true
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  routes.forEach((route) => {
    if (route.name === to.name) {
      if (route.requiresAuth) {
        store.dispatch('onAuthChange', (user) => {
          if (user) {
            next()
          } else {
            router.push(authRoutePath)
          }
        })
      } else {
        next()
      }
    }
  })
})

export default router
