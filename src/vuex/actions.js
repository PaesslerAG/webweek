import firebase from 'firebase'

export default {
  signin: ({ state, commit }, form) => {
    if (!state.user) {
      return firebase.auth().signInWithEmailAndPassword(form.email, form.password).then((user) => {
        commit('SET_USER', user)
      })
    }

    return Promise.resolve(state.user)
  },

  signup: ({ state, commit }, form) => firebase.auth().createUserWithEmailAndPassword(form.email, form.password).then((user) => {
    state.usersRef.push({ uid: user.uid, email: form.email, name: form.username, displayName: form.username })
    firebase.auth().currentUser.updateProfile({
      displayName: form.username
    }).then(commit('SET_USER', user))
  }),

  updateUser: ({ commit }, user) => {
    commit('SET_USER', user)
  },

  logout: ({ commit }) => {
    commit('REMOVE_USER')
    return firebase.auth().signOut()
  },

  updatePassword: ({ commit }, newPassword) => {
    commit('CHANGE_PASSWORD')
    return firebase.auth().currentUser.updatePassword(newPassword)
  },

  onAuthChange: ({ commit }, callback) => {
    firebase.auth().onAuthStateChanged((user) => {
      commit('AUTH_CHANGE', user)
      callback(user)
    })
  }
}
