export default {
  openProjects: state => state.projects.filter(project => !project.completed),

  completedProjects: state => state.projects.filter(project => project.completed).length
}
