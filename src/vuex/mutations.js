import Vue from 'vue'

export default {
  SET_USER(state, user) {
    // window.localStorage.setItem('user', JSON.stringify(user))
    Vue.set(state, 'user', user)
  },

  UPDATE_USER(state, user) {
    // window.localStorage.setItem('user', JSON.stringify(user))
    Vue.set(state, 'user', user)
  },

  REMOVE_USER(state) {
    // window.localStorage.removeItem('user')
    Vue.set(state, 'user', null)
  },

  AUTH_CHANGE(state, user) {
    Vue.set(state, 'user', user)
  },

  CHANGE_PASSWORD(state) {
    Vue.set(state)
  }
}
