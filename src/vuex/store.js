import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'
import firebasedb from '../firebasedb'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    db: firebasedb,
    usersRef: firebasedb.ref('users'),
    projectsRef: firebasedb.ref('projects'),
    skillsRef: firebasedb.ref('skills'),

    user: firebasedb.user,
    users: [],
    projects: [],
    skills: [],
    debug: false
  },

  actions,
  mutations,
  getters
})

export default store
