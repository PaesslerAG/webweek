
module.exports = {
  'Project page test': function (browser) {
    const pageUrl = `${browser.globals.devServerURL}/#/projects`

    browser
      .url(pageUrl)
      .pause(1000);

    // expect element  to be present in 1000ms
    browser.expect.element('body').to.be.present.before(1000);

    // expect element <#lst-ib> to have css property 'display'
    browser.expect.element('li.is-active').text.to.contain('Active Projects');
    browser.expect.element('article:first-child h2').text.to.contain('WebWeek 2017');

    // browser.expect.element('li.is-active').to.have.contain('display');
    // // browser.expect.element('li.is-active').to.have.css('display');
    //
    // // expect element  to have attribute 'class' which contains text 'vasq'
    // browser.expect.element('body').to.have.attribute('class').which.contains('vasq');
    //
    // // expect element <#lst-ib> to be an input tag
    // browser.expect.element('#lst-ib').to.be.an('input');
    //
    // // expect element <#lst-ib> to be visible
    // browser.expect.element('#lst-ib').to.be.visible;

    browser.end();
  },
};
