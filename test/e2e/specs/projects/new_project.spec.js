
module.exports = {
  'Project page test': function (browser) {
    const pageUrl = `${browser.globals.devServerURL}/#/projects`

    browser.url(pageUrl)
    browser.waitForElementVisible('body', 1000)
    browser.click('a.button.is-success')

    // browser.expect.element('body').to.be.present.before(1000);
    browser.expect.element('.card-header-title').text.to.contain('Add new project');

    // browser.expect.element('li.is-active').to.have.contain('display');
    // // browser.expect.element('li.is-active').to.have.css('display');
    //
    // // expect element  to have attribute 'class' which contains text 'vasq'
    // browser.expect.element('body').to.have.attribute('class').which.contains('vasq');
    //
    // // expect element <#lst-ib> to be an input tag
    // browser.expect.element('#lst-ib').to.be.an('input');
    //
    // // expect element <#lst-ib> to be visible
    // browser.expect.element('#lst-ib').to.be.visible;

    browser.end();
  },
};
