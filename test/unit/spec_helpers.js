/* eslint no-proto: "error"*/
export default function mock(object, name) {
  const keys = Object.keys(object.__proto__)
  return keys.length > 0 ? jasmine.createSpyObj(name || 'mock', keys) : {};
}
