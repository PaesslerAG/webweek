import Vue from 'vue';
import Hello from 'src/components/Hello';

before(() => {
  const Constructor = Vue.extend(Hello);
  this.component = new Constructor().$mount();
})

describe('Hello.vue', () => {
  it('renders correct contents', () => {
    expect(this.component.$el.querySelector('.hello h1').textContent).to.equal('Welcome to Your Vue.js App')
  })
})
