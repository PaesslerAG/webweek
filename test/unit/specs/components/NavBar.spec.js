import Vue from 'vue'
import NavBar from 'src/components/NavBar'
import store from 'src/vuex/store'
import router from '../../factories/router'

describe('NavBar', () => {
  describe('- component', () => {
    it('sets the correct default data', () => {
      const defaultData = NavBar.data()

      expect(defaultData.mobileMenuIsOpen).to.be.false
    })
  })

  describe('- mounted', () => {
    before(() => {
      const Constructor = Vue.extend({ ...NavBar, store, router });
      this.component = new Constructor().$mount()
      this.initState = Object.assign({}, store.state)
    })

    after(() => {
      this.component.$store.replaceState(this.initState)
    })

    describe('computed', () => {
      describe('.user', () => {
        it('references store user', () => {
          this.component.$store.state.user = { uid: 'test', displayName: 'Test User' }

          expect(this.component.user.displayName).to.be.equal('Test User')
        })
      })

      describe('.isAuthenticated', () => {
        it('returns true if user authenticated', () => {
          this.component.$store.state.user = { uid: 'test', displayName: 'Test User' }

          expect(this.component.isAuthenticated).to.be.true
        })

        it('returns false if not', () => {
          this.component.$store.state.user = null

          expect(this.component.isAuthenticated).to.be.false
        })
      })

      describe('.currentRoute', () => {
        it('references current route', () => {
          sinon.stub(this.component, '$route', {
            get() {
              return { path: '/projects' }
            }
          })

          expect(this.component.currentRoute).to.equal('/projects')
        })
      })
    })

    describe('methods', () => {
      describe('logout', () => {
        it('redirects to root path', () => {
          const routerSpy = sinon.spy(this.component.$router, 'push')

          this.component.logout()

          expect(routerSpy).to.have.been.calledWith('/')
        })

        it('dispatches logout action', () => {
          const storeSpy = sinon.spy(this.component.$store, 'dispatch')

          this.component.logout()

          expect(storeSpy).to.have.been.calledWith('logout')
        })
      })

      describe('toggleMobileMenu', () => {
        it('toggles mobileMenuIsOpen data attribute', () => {
          this.component.mobileMenuIsOpen = true

          this.component.toggleMobileMenu()

          expect(this.component.mobileMenuIsOpen).to.equal(false)
        })
      })
    })
  })
})
