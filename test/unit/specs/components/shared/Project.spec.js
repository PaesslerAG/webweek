import Vue from 'vue'
import Project from 'src/components/shared/Project'
import store from 'src/vuex/store'

describe('Project', () => {
  describe('- component', () => {
    it('has child components', () => {
      expect(Object.values(Project.components).map(c => c.name)).to.eql(['Tag'])
    })

    it('sets the correct default data', () => {
      const defaultData = Project.data()
      expect(defaultData.form).to.be.false
    })

    it('sets the correct default props', () => {
      expect(Project.props).to.have.all.keys(['project'])
      expect(Project.props.project.default()).to.have.all.keys('skills', 'attendee_ids')
    })

    it('has a created hook', () => {
      expect(Project.created).to.be.an('function')
    })
  })

  describe('- mounted', () => {
    beforeEach(() => {
      const project = { name: 'test', description: 'Test project', skills: null }
      const Constructor = Vue.extend({ ...Project, store, prop: { project } })

      this.component = new Constructor().$mount()
      this.initState = Object.assign({}, store.state)
    })

    afterEach(() => {
      this.component.$destroy()
      this.component.$store.replaceState(this.initState)
    })

    it('converts skills and attendee_ids to array if empty', () => {
      expect(this.component.project.skills).to.be.eql([])
      expect(this.component.project.attendee_ids).to.be.eql([])
    })

    describe('computed', () => {
      describe('.user', () => {
        it('references user store state', () => {
          this.component.$store.state.user = { uid: 'test', displayName: 'Test User' }

          expect(this.component.user.displayName).to.be.equal('Test User')
        })
      })

      describe('.owner', () => {
        it('returns project owner object', () => {
          const user = { uid: 1, displayName: 'Test user' }
          this.component.users = [user]
          this.component.project.owner_id = 1

          expect(this.component.owner).to.be.eql(user)
        })

        it('returns empty object if no owner', () => {
          this.component.users = []
          this.component.project.owner_id = 111
          expect(this.component.owner).to.be.eql({})
        })
      })

      describe('.attendee_ids', () => {
        it('returns project attendee_ids', () => {
          this.component.project.attendee_ids = [1]
          expect(this.component.attendee_ids).to.be.eql([1])
        })

        it('returns empty array if project attendee_ids is undefined', () => {
          this.component.project.attendee_ids = undefined
          expect(this.component.attendee_ids).to.be.eql([])
        })
      })

      describe('.attendees', () => {
        it('returns project attendees', () => {
          this.component.users = [{ uid: 1 }, { uid: 2 }, { uid: 3 }]
          this.component.project.attendee_ids = [1, 2]

          expect(this.component.attendees).to.be.eql([{ uid: 1 }, { uid: 2 }])
        })

        it('returns empty array if no attendees', () => {
          this.component.users = [{ uid: 1 }, { uid: 2 }, { uid: 3 }]
          this.component.project.attendee_ids = []

          expect(this.component.attendee_ids).to.be.eql([])
        })
      })

      describe('.joinable', () => {
        it('returns true if current user can join project', () => {
          this.component.$store.state.user = { uid: 100 }

          expect(this.component.joinable).to.be.true
        })

        it('returns false if current user is a project owner', () => {
          this.component.$store.state.user = { uid: 1 }
          this.component.project.owner_id = 1

          expect(this.component.joinable).to.be.false
        })

        it('returns false if current user already joined the project', () => {
          this.component.$store.state.user = { uid: 100 }
          this.component.project.attendee_ids = [100]

          expect(this.component.joinable).to.be.false
        })
      })

      describe('.leaveable', () => {
        it('returns true if current user can leave project', () => {
          this.component.$store.state.user = { uid: 100 }
          this.component.project.attendee_ids = [100]

          expect(this.component.leaveable).to.be.true
        })

        it('returns false if current user is not a project attendee', () => {
          this.component.$store.state.user = { uid: 1 }
          this.component.project.attendee_ids = []

          expect(this.component.leaveable).to.be.false
        })
      })

      describe('.editable', () => {
        it('returns true if current user is project owner', () => {
          this.component.project.owner_id = 1
          this.component.$store.state.user = { uid: 1 }

          expect(this.component.editable).to.be.true
        })

        it('returns false if not', () => {
          this.component.project.owner_id = 2
          this.component.$store.state.user = { uid: 1 }

          expect(this.component.editable).to.be.false
        })
      })
    })

    describe('methods', () => {
      describe('.edit()', () => {
        it('emits edit event', () => {
          const emitSpy = sinon.spy(this.component, '$emit')

          this.component.edit()

          expect(emitSpy).to.have.been.calledWith('edit')
        })
      })

      describe('.join()', () => {
        it('adds user to project attendees if project is joinable', () => {
          const updateSpy = sinon.stub(this.component, 'update').returns()
          this.component.$store.state.user = { uid: 1 }

          this.component.join()

          expect(this.component.attendee_ids).to.include(this.component.user.uid)
          expect(updateSpy).to.have.been.called
        })

        it('skips if project is not joinable', () => {
          const updateSpy = sinon.stub(this.component, 'update').returns()
          this.component.$store.state.user = { uid: 1 }
          this.component.project.owner_id = this.component.user.uid

          this.component.join()

          expect(this.component.attendee_ids).to.be.empty
          expect(updateSpy).to.not.have.been.called
        })
      })

      describe('.leave()', () => {
        it('remove user from attendees if project is leaveable', () => {
          const updateSpy = sinon.stub(this.component, 'update').returns()
          this.component.$store.state.user = { uid: 1 }
          this.component.project.attendee_ids = [this.component.user.uid]

          this.component.leave()

          expect(this.component.attendee_ids).to.not.include(this.component.user.uid)
          expect(updateSpy).to.have.been.called
        })

        it('skips if project is not leaveable', () => {
          const updateSpy = sinon.stub(this.component, 'update').returns()
          this.component.$store.state.user = { uid: 1 }
          this.component.project.attendee_ids = []

          this.component.leave()

          expect(updateSpy).to.not.have.been.called
        })
      })

      describe('.update()', () => {
        it('updates a project', () => {
          this.component.$firebaseRefs = { projects: { child() { } } }
          const updateSpy = sinon.stub(this.component.$firebaseRefs.projects, 'child').returns({ update: () => {} })

          this.component.update()

          expect(updateSpy).to.been.called
        })
      })

      describe('.complete()', () => {
        it('sets project status and updates a project', () => {
          const updateSpy = sinon.stub(this.component, 'update').returns()

          this.component.complete(true)

          expect(this.component.project.completed).to.be.true
          expect(updateSpy).to.have.been.called
        })
      })

      describe('.remove()', () => {
        it('deletes a project', () => {
          this.component.$firebaseRefs = { projects: { child() { } } }
          const removeSpy = sinon.stub(this.component.$firebaseRefs.projects, 'child').returns({ remove: () => {} })

          this.component.remove()

          expect(removeSpy).to.been.called
        })
      })
    })
  })
})
