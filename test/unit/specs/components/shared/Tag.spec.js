import Vue from 'vue';
import Tag from 'src/components/shared/Tag';

// Aufgabe #7
describe.only('Tag.vue', () => {
  before(() => {
    this.parent = new Vue({
      template: '<tag ref="testComponent">WebWeek 2017</tag>',
      components: { Tag }
    }).$mount()

    this.component = this.parent.$refs.testComponent
  })

  it('sets the correct default data', () => {
    expect('Tag.???.colorSet').to.eql({ green: 'is-primary', blue: 'is-info' })
  })

  it('sets the correct default props', () => {
    expect('Tag.????').to.have.all.keys('color')
  })

  it('renders correct content', () => {
    expect(this.component.$el.textContent.trim()).to.equal('???')
    expect(this.component.$el.className).to.include('???')
  })
})
