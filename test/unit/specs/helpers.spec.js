import omit from 'src/helpers'

before(() => {
  this.testObject = { a: 1, b: 2, c: 3 }
})

describe('omit()', () => {
  it('removes key and returns object copy', () => {
    expect(omit(this.testObject, 'a')).to.deep.equal({ b: 2, c: 3 })
  })

  it('keeps the origin object untouched', () => {
    omit(this.testObject, 'c')

    expect(this.testObject).have.all.keys('a', 'b', 'c')
  })
})
